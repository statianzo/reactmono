import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ThumbsUp from 'components/ThumbsUp'

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Open up App.js to start building your app!</Text>
      <ThumbsUp />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

import * as React from 'react'
import {View, Text} from 'react-native'

const ThumbsUp = () => {
  return (
    React.createElement(
      View,
      null,
      React.createElement(Text, null, 'I approved')
    )
  )
}

export default ThumbsUp
